﻿const e = React.createElement;

if (!containerWidgetRates) {
    throw "My exception - containerWidgetRates not found";
}

class Widget extends React.Component {
    constructor(props) {
        super(props);
        this.privatUrl = "https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5";

        this.state = {
            rates: []
        };

        this.createElementForBuy = this.createElementForBuy.bind(this);
        this.createElementForSale = this.createElementForSale.bind(this);
    }

    componentDidMount() {
        fetch(this.privatUrl)
            .then(r => r.json())
            .then(j => {
                this.setState({ rates: j });
            });
    }

    getRates(url) {
        let rate = [];
        fetch(url)
            .then(prom => prom.json())
            .then(res => {
                res.forEach(el => {
                    rate.push({
                        "ccy": el.ccy,
                        "buy": el.buy,
                        "sale": el.sale
                    });
                });
            });
        this.setState(st => { st.rates = rate; return st; });
    }

    getA(arr) {
        let rate = [];
        arr.forEach(el => {
            rate.push(Object.assign({}, el));
        });
        return rate;
    }

    render() {
        return e('div', {},
            e('table', { className: 'Table' },
                e('tbody', {},
                    e('tr', {},
                        e('td', { colSpan: "2" }, e('img', { src: "img/64/EUR64.png" }, null)),
                        e('td', { colSpan: "2" }, e('img', { src: "img/64/USD64.png" }, null)),
                        e('td', { colSpan: "2" }, e('img', { src: "img/64/RUB64.png" }, null)),
                        e('td', { colSpan: "2" }, e('img', { src: "img/64/BITCOIN64.png" }, null))
                    ),
                    e('tr', {},
                        e('td', {}, "Sale"),
                        e('td', {}, "Buy"),
                        e('td', {}, "Sale"),
                        e('td', {}, "Buy"),
                        e('td', {}, "Sale"),
                        e('td', {}, "Buy"),
                        e('td', {}, "Sale"),
                        e('td', {}, "Buy")
                    ),
                    e('tr', {},
                        this.createElementForSale(1),
                        this.createElementForBuy(1),
                        this.createElementForSale(0),
                        this.createElementForBuy(0),
                        this.createElementForSale(2),
                        this.createElementForBuy(2),
                        this.createElementForSale(3),
                        this.createElementForBuy(3),
                    )))
        );
    }

    createElementForBuy(index) {
        return e('td', {}, Number(Object(this.state.rates[index]).buy).toFixed(2));
    }

    createElementForSale(index) {
        return e('td', {}, Number(Object(this.state.rates[index]).sale).toFixed(2));
    }
}

ReactDOM.render(e(Widget, {}, {}), document.getElementById("containerWidgetRates"));