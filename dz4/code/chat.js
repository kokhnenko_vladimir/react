﻿const cont = document.getElementById('containerChat');

if (!cont) {
    throw "My exception - containerChat not found";
}

class Chat extends React.Component {
    render() {
        return <>
            <div id="chat">
                <label className="nickBox">Nick</label>
                <input type="text" id="nick" className="textBox"></input>
                <div id='showMessages'>
                    <p className="name">Name</p>
                    <p className="text">super long txt</p>
                    <p className="name">Name</p>
                    <p className="text">super long txt</p>
                    <p className="name">Name</p>
                    <p className="text">
                        super long txt ssssssssssssss sssssssssssssssssssssss ssssssssssssssssssssssssssss sssssssssssssssssssssssssssssssssss
                        super long txt ssssssssssssss sssssssssssssssssssssss ssssssssssssssssssssssssssss sssssssssssssssssssssssssssssssssss
                        super long txt ssssssssssssss sssssssssssssssssssssss ssssssssssssssssssssssssssss sssssssssssssssssssssssssssssssssss
                        super long txt ssssssssssssss sssssssssssssssssssssss ssssssssssssssssssssssssssss sssssssssssssssssssssssssssssssssss
                    </p>
                    <p className="name">Name</p>
                    <p className="text">
                        super long txt ssssssssssssss sssssssssssssssssssssss ssssssssssssssssssssssssssss sssssssssssssssssssssssssssssssssss
                        super long txt ssssssssssssss sssssssssssssssssssssss ssssssssssssssssssssssssssss sssssssssssssssssssssssssssssssssss
                        super long txt ssssssssssssss sssssssssssssssssssssss ssssssssssssssssssssssssssss sssssssssssssssssssssssssssssssssss
                        super long txt ssssssssssssss sssssssssssssssssssssss ssssssssssssssssssssssssssss sssssssssssssssssssssssssssssssssss
                    </p>
                    <p className="name">Name</p>
                    <p className="text">
                        super long txt ssssssssssssss sssssssssssssssssssssss ssssssssssssssssssssssssssss sssssssssssssssssssssssssssssssssss
                        super long txt ssssssssssssss sssssssssssssssssssssss ssssssssssssssssssssssssssss sssssssssssssssssssssssssssssssssss
                        super long txt ssssssssssssss sssssssssssssssssssssss ssssssssssssssssssssssssssss sssssssssssssssssssssssssssssssssss
                        super long txt ssssssssssssss sssssssssssssssssssssss ssssssssssssssssssssssssssss sssssssssssssssssssssssssssssssssss
                    </p>
                </div>
                <textarea id="message"></textarea>
                <p className="send">
                    <button>Send</button>
                </p>

            </div>
        </>;
    }
}

ReactDOM.render(<Chat />, cont);