/*
    Получить курсы валют и отобразить в виде виджета
    "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json"
*/

const contWidgetNatsbank = document.querySelector("#containerWidgetRatesNatsbank"); // DOM container
if (!contWidgetNatsbank) {
    throw "container 'id-containerWidgetRatesNatsbank' not found";
}

class RatesNatsbank extends React.Component {
    constructor(props) {
        super(props);
        this.ratesURL = "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json";
        this.state = {
            rates: [],
            shown: props.shown
        };
        this.saveRates = this.saveRates.bind(this);
    }

    saveRates(j) {
        let res = [];
        for (let i = 0; i < j.length; i++) {
            if (this.state.shown.includes(j[i].cc)) {
                res.push(j[i]);
            }
        }
        this.setState(st => { st.rates = res; /* console.log(j); // для проверки */  return st; })
    }

    componentDidMount() { // componentWillMount - перед встраиванием в DOM
        // запрос курса валют
        // React не переопределяет AJAX функционал - берем стандартный (window.fetch)
        fetch(this.ratesURL)          // запрос на URL методом GET
            .then(                    // Promise интерфейс
                //r => r.text()       // Извлечь тело ответа как текст
                r => r.json()         // Извлечь тело ответа как JSON
            )
            .then(                    // Результат извлечения тела ответа сервера (URL)
                //console.log  так и пишется без скобок, можно посмотреть что пришло

                // j => this.setState({ // Стрелочная нотация - захватывает (замыкает) this
                //     rates: j
                // })

                // ~ function(j) {/*this нужно замкнуть из вне */ this.setState(st => {st.rates = j; return st;})} аналог
                // вышенаписанной стрелочной функции, разница в том что this в этой функции 
                // обращается к промису, а в стрелочной функции используется замыкание указателя 
                // this на класс Rates

                this.saveRates //- это прямой аналог с стрелочной функции 17.12.2020 10:53

            )
    }

    render() {
        return e('div', {},
        e('h3', {}, "Курс валют нацбанка"),
        e('ol', {},
            this.state.rates.map(this.createElementFromNBURate)
        ));
    }

    createElementFromNBURate(r) {
        // NBU r: {r030: 124, txt: "Канадський долар", rate: 21.765, cc: "CAD", exchangedate: "17.12.2020"}
        return React.createElement('li', { key: r.r030 }, r.rate + " ", r.cc + " ( " + r.txt + " )");
    }
}

ReactDOM.render(e(RatesNatsbank, { shown: ['GBP', 'USD', 'EUR', 'AED'] }, null), contWidgetNatsbank);

