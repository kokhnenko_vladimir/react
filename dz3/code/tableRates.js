/*
    Получить курсы валют и отобразить в виде таблицы
    "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json"
*/

const contTableRates = document.querySelector("#containerTableRates"); // DOM container
if (!contTableRates) {
    throw "container 'id-containerTableRates' not found";
}


// const e = React.createElement;

class RatesTable extends React.Component {
    constructor(props) {
        super(props);
        this.ratesURL = "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json";
        this.state = {
            rates: []
        };
    }

    componentDidMount() {
        fetch(this.ratesURL).then(r => r.json()).then(j => this.setState({ rates: j }));
    }

    render() {
        return e('div', {},
            e('h3', {}, "Таблица курсов валют нацбанка"),
            e('table', { className: 'table' },
                e('tbody', {},
                    this.state.rates.map(this.createElementFromNBURate)
                )));
    }

    createElementFromNBURate(r, i) {
        // NBU r: {r030: 124, txt: "Канадський долар", rate: 21.765, cc: "CAD", exchangedate: "17.12.2020"}
        return e('tr', { key: r.r030 },
            e('td', {}, i + '.'),
            e('td', {}, r.cc),
            e('td', {}, r.rate),
            e('td', {}, r.txt)
        );
    }
}

ReactDOM.render(e(RatesTable, {}, null), contTableRates);

