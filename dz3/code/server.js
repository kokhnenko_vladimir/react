const http = require('http');
const fs = require('fs');
const PORT = '80';
const HOST = '127.0.0.1';

http.createServer(serverCore)
    .listen(PORT, HOST, () => {
        console.log("Server listen port " + PORT);
    });

function serverCore(request, response) {

    let url = decodeURI(request.url.substr(1));
    let method = request.method.toUpperCase();
    console.log(method + ' ' + url);

    switch (url) {
        case '':
        case 'index.html':
            pipeFile('index.html', 'text/html', response);
            break;

        case 'css/style.css':
            pipeFile('css/style.css', 'text/css', response);
            break;

        case 'favicon.ico':
            pipeFile('img/react-icon.png', 'image/png', response);
            break;

        case 'img/64/EUR64.png':
            pipeFile('img/64/EUR64.png', 'image/png', response);
            break;
        case 'img/64/USD64.png':
            pipeFile('img/64/USD64.png', 'image/png', response);
            break;
        case 'img/64/BITCOIN64.png':
            pipeFile('img/64/BITCOIN64.png', 'image/png', response);
            break;
        case 'img/64/RUB64.png':
            pipeFile('img/64/RUB64.png', 'image/png', response);
            break;

        case 'scripts/react.development.js':
            pipeFile('scripts/react.development.js', 'text/javascript', response);
            break;

        case 'scripts/react-dom.development.js':
            pipeFile('scripts/react-dom.development.js', 'text/javascript', response);
            break;

        case 'code/widgetPrivat.js':
            pipeFile('code/widgetPrivat.js', 'text/javascript', response);
            break;

        case 'code/widgetRatesNatsbank.js':
            pipeFile('code/widgetRatesNatsbank.js', 'text/javascript', response);
            break;

        case 'code/tableRates.js':
            pipeFile('code/tableRates.js', 'text/javascript', response);
            break;

        default:
            pipeFile('index.html', 'text/html', response);
            break;
    }
}


async function pipeFile(name, type, response) {

    var f = fs.createReadStream(name);

    f.on('open', () => {
        response.writeHead(200, {
            "Content-Type": type,
            "Connection": "close"
        });
        f.pipe(response);
    });
}