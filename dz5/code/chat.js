﻿const cont = document.body.querySelector('#containerChat');

if (!cont) {
    throw "My exception - containerChat not found";
}

class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nik: '',
            message: '',
            msgs: []
        };
        
        this.timerUpdateId = '';

        this.onMsgChange = this.onMsgChange.bind(this);
        this.onNikChange = this.onNikChange.bind(this);
        this.sendClick = this.sendClick.bind(this);
        this.updateChat = this.updateChat.bind(this);
    }

    onMsgChange(evt) {
        this.setState({ message: evt.target.value });
    }

    onNikChange(evt) {
        this.setState({ nik: evt.target.value });
    }

    sendClick() {
        // console.log(this.state.nik + ' : ' + this.state.message);

        if (this.state.nik.length == 0 || this.state.message.length == 0) return;

        fetch('https://chat.momentfor.fun?author=' + this.state.nik + '&msg=' + this.state.message)
            .then(r => r.json())
            .then(j => {
                if (j.status != 1) {
                    alert('Server error');
                    return;
                }
                this.setState({ msgs: j.data });
            })
        message.value = '';
        this.setState({ message: '' });
    }

    updateChat() {
        fetch('https://chat.momentfor.fun')
            .then(r => r.json())
            .then(j => {
                if (j.status != 1) {
                    alert('Server error');
                    return;
                }
                this.setState({ msgs: j.data });
            });
        // console.log('chat updating');
    }

    componentDidMount() {
        this.timerUpdateId = setInterval(this.updateChat, 1000);
    }

    componentWillUnmount() {
        clearInterval(this.timerUpdateId);
    }

    render() {
        return <>
            <div id="chat">
                <label className="nickBox">Nick</label>
                <input type="text" id="nick" className="textBox" onChange={this.onNikChange}></input>
                <div id='showMessages'>
                    {
                        this.state.msgs.map((m, i) => {
                            return <span key={m.id}>
                                <p className="name">{m.author}</p>
                                <p className="text">{m.text}</p>
                            </span>
                        })
                    }
                </div>
                <textarea id="message" onChange={this.onMsgChange}></textarea>
                <p className="send" onClick={this.sendClick}>
                    <button>Send</button>
                </p>
            </div>
        </>;
    }
}

ReactDOM.render(<Chat />, cont);